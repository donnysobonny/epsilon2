﻿using Epsilon.Commands.BuiltIn;
using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Epsilon {
    public class Program : CommandBase {
        static void Main() {
            new Program();
        }

        public Program() {
            Core c = Core.instance;
            c.initiate();

            //if first time running, run tutorial
            if(c.cachedMeta.firstTimeRunning) {
                new Tutorial().start("");
            } else {
                //start off in idle
                this.idle();
            }

            while (!Console.KeyAvailable) {
                Thread.Sleep(100);
            }
        }

        public override void awake() {
            throw new NotImplementedException();
        }

        public override string[] getCommandPhrases() {
            throw new NotImplementedException();
        }

        public override void start(string phrase) {
            throw new NotImplementedException();
        }

        public override void update() {
            throw new NotImplementedException();
        }

        public override string getDescription(string phrase) {
            throw new NotImplementedException();
        }
    }
}
