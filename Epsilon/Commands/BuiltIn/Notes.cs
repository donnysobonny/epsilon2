﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Epsilon.Commands.BuiltIn {
    public class Notes : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "create note",
                "edit note",
                "delete note",
                "read note",
                "list notes"
            };
        }

        public override void start(string phrase) {
            switch (phrase) {
                case "create note":
                    this.createNote();
                    break;
                case "edit note":
                    this.editNote();
                    break;
                case "delete note":
                    this.deleteNote();
                    break;
                case "read note":
                    this.readNote();
                    break;
                case "list notes":
                    this.listNotes();
                    break;
            }
        }

        private void listNotes() {
            this.speak("The currently stored notes are: ");
            this.speakDataList(new Helpers.DataList("Note", "name", 8, "id DESC"), delegate () {
                this.speak("I have finished listing the stored notes.");
                this.end();
            });
        }

        private void createNote() {
            this.speak_ListenForAnything("Please give your note a name.", delegate (string text) {
                this.confirmPhrase_ListenForAcceptance(text, delegate (bool acceptance) {
                    if (acceptance) {
                        if (this.noteExistsData(text)) {
                            this.speak_ListenForAcceptance("There is already a note with this name. Would you like to try again?", delegate (bool acceptance2) {
                                if (acceptance2) {
                                    this.createNote();
                                } else {
                                    this.speakCategory("okay");
                                    this.end();
                                }
                            });
                        } else {
                            this.createNoteData(text);
                            this.speak_ListenForAnything("Please say the contents of the note.", delegate (string body) {
                                this.setBodyData(text, body);
                                this.speak("Your note has successfully been created.");
                                this.end();
                            }, "I am waiting for you to say the body of the note.", false);
                        }
                    } else {
                        this.createNote();
                    }
                });
            }, "I am waiting for your to tell me the name of the new note.");
        }

        private void editNote() {
            this.speak_listenForDataList("Please tell me the name of the note.", new Helpers.DataList("Note", "name", 1000, "id DESC"), delegate (string name) {
                if (!this.noteExistsData(name)) {
                    this.speak_ListenForAcceptance("I could not find a note with the name: " + name + ". Do you want to try again?", delegate (bool accepted) {
                        if (accepted) {
                            this.editNote();
                        } else {
                            this.speakCategory("okay");
                            this.end();
                        }
                    });
                } else {
                    this.speak_ListenForAnything("Please say the new contents of the note.", delegate (string body) {
                        this.setBodyData(name, body);
                        this.speak("Your note has successfully been updated.");
                        this.end();
                    }, "I am waiting for you to say the body of the note.", false);
                }
            }, "I am waiting for you to tell me the name of the note that you want to edit.");
        }

        private void deleteNote() {
            this.speak_listenForDataList("Please tell me the name of the note.", new Helpers.DataList("Note", "name", 1000, "id DESC"), delegate (string name) {
                if (!this.noteExistsData(name)) {
                    this.speak_ListenForAcceptance("I could not find a note with the name: " + name + ". Do you want to try again?", delegate (bool accepted) {
                        if (accepted) {
                            this.deleteNote();
                        } else {
                            this.speakCategory("okay");
                            this.end();
                        }
                    });
                } else {
                    this.speak_ListenForAcceptance("Are you sure you want to delete this note?", delegate (bool acceptance) {
                        if (acceptance) {
                            this.deleteNoteData(name);
                            this.speak("Your note was successfully deleted.");
                            this.end();
                        } else {
                            this.end();
                        }
                    }, "I am waiting for you to confirm that you want to delete this note.");
                }
            }, "I am waiting for you to tell me the name of the note that you want to delete.");
        }

        private void readNote() {
            this.speak_listenForDataList("Please tell me the name of the note.", new Helpers.DataList("Note", "name", 1000, "id DESC"), delegate (string name) {
                if (!this.noteExistsData(name)) {
                    this.speak_ListenForAcceptance("I could not find a note with the name: " + name + ". Do you want to try again?", delegate (bool accepted) {
                        if (accepted) {
                            this.readNote();
                        } else {
                            this.speakCategory("okay");
                            this.end();
                        }
                    });
                } else {
                    this.speak(this.getBodyData(name));
                    this.end();
                }
            }, "I am waiting for you to tell me the name of the note that you want to read.");
        }

        public override void update() {

        }

        private bool noteExistsData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT id FROM Note WHERE name = @1 LIMIT 1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            SQLiteDataReader r = c.ExecuteReader();
            while (r.Read()) {
                return true;
            }
            return false;
        }

        private void createNoteData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "INSERT INTO Note (name,body) VALUES (@1,'')";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            c.ExecuteNonQuery();
        }

        private void deleteNoteData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "DELETE FROM Note WHERE name = @1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            c.ExecuteNonQuery();
        }

        private string getBodyData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT body FROM Note WHERE name = @1 LIMIT 1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            SQLiteDataReader r = c.ExecuteReader();
            while (r.Read()) {
                return (string)r["body"];
            }
            return "";
        }

        private void setBodyData(string name, string body) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "UPDATE Note SET body = @1 WHERE name = @2";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", body));
            c.Parameters.Add(new SQLiteParameter("@2", name));
            c.ExecuteNonQuery();
        }

        public override string getDescription(string phrase) {
            switch (phrase) {
                case "create note":
                    return "This command is used to create new notes.";
                case "edit note":
                    return "This command is used to edit an existing note.";
                case "delete note":
                    return "This command is used to delete an existing note.";
                case "read note":
                    return "This command is used to read an existing note.";
                case "list notes":
                    return "This command is used to list all notes";
            }
            return "This command is used to create, read, edit and delete notes.z";
        }
    }
}
