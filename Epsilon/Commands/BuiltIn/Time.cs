﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class Time : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "what's the time",
                "what is the time",
                "what time is it",
                "tell me the time",
            };
        }

        public override string getDescription(string phrase) {
            return "This simple command can be used to tell you the time!";
        }

        public override void start(string phrase) {
            this.speakRandom(new string[] {
                "The time is currently:",
                "The current time is:"
            });
            string time = DateTime.Now.ToString(@"h:mm tt");
            this.speak(time + ".");
            this.end();
        }

        public override void update() {

        }
    }
}
