﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class YourName : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "what's your name",
                "what is your name",
                "tell me your name",
                "set your name",
                "get your name"
            };
        }

        public override void start(string phrase) {
            string name = this.getComputerName();
            this.speakRandom(new string[] {
                "My name is " + name + ", of course!",
                "You know my name! It's " + name + ".",
                "You don't know my name? It's " + name + "."
            });
            this.speak_ListenForAcceptance("Would you like me to change it?", delegate (bool acceptance) {
                if (acceptance) {
                    this.changeName();
                } else {
                    this.speakCategory("okay");
                    this.end();
                }
            }, "I am waiting for you to tell me whether you want to change my name.");
        }

        private void changeName() {
            this.speak_ListenForAnything("What would you like me to change it to?", delegate (string text) {

                this.confirmPhrase_ListenForAcceptance(text, delegate (bool acceptance) {
                    if (acceptance) {
                        this.speakCategory("okay");
                        this.speak("My new name is " + text + "!");
                        this.setComputerName(text);
                        this.end();
                    } else {
                        this.changeName();
                    }
                });
            }, "I am waiting for you to tell me my new name.");
        }

        public override void update() {

        }

        public override string getDescription(string phrase) {
            return "This command is used to change my name. If can be accessed in a number of ways. For example, try saying: what's your name? or: tell me your name.";
        }
    }
}
