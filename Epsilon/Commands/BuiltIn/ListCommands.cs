﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class ListCommands : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "list commands",
                "all commands"
            };
        }

        public override string getDescription(string phrase) {
            return "This command is used to list all currently installed commands.";
        }

        public override void start(string phrase) {
            this.speak("The currently installed commands are: ");
            this.speakList(Core.instance.cachedCommandPhrases, delegate() {
                this.speak("I have finished listing the commands.");
                this.end();
            }, 8);
        }

        public override void update() {

        }
    }
}
