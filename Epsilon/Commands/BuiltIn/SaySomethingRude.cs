﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class SaySomethingRude : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "say something rude"
            };
        }

        public override string getDescription(string phrase) {
            return "Thrust me. You do not want to know what this command does!";
        }

        public override void start(string phrase) {
            this.speakRandom_ListenForAcceptance(new string[] {
                "Are you sure about this?",
                "Are you sure?",
                "I'm not so sure about this. Are you sure?",
                "Do I have to?",
                "Oh god, not this again. Are you sure?"
            }, delegate (bool acceptance) {
                if (acceptance) {
                    this.speak("Okay, Here I go. ");
                    this.speakRandom(new string[] {
                        "fuck!",
                        "cock sucker!",
                        "i love big balls!",
                        "i haven't shaved in months!",
                        "don't touch my bum!",
                        "i don't like floppy disks!",
                        "memory is not ram!",
                        "pants!",
                        "elbow!"
                    });
                    this.end();
                } else {
                    this.speakCategory("okay");
                    this.end();
                }
            });
        }

        public override void update() {

        }
    }
}
