﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Epsilon.Commands.BuiltIn {
    public class TimeOut : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "quiet for a minute",
                "go sit in the corner",
                "restart my computer",
                "done for now",
                "omega shutdown protocol",
                "example with range"
            };
        }
        public override void start(string phrase) {
            switch (phrase) {
                case "quiet for a minute":
                    this.stopwatch();
                    break;
                case "qo sit in the corner":
                    this.timeout();
                    break;
                case "restart my computer":
                    this.reboot();
                    break;
                case "done for now":
                    this.punchout();
                    break;
                case "omega shutdown protocol":
                    this.clockout();
                    break;
                case "example with range":
                    this.example();
                    break;
            }
        }

        /// <summary>
        /// This example shows how to make the wait time configurable with a number range
        /// </summary>
        private void example() {
            this.speak_ListenForNumberRange("For many seconds?", 1, 60, delegate (int num) {
                this.speak("I'll let you know when i'm back");
                System.Threading.Thread.Sleep(num * 1000);
                this.speak("I'm back!");
                this.end();
            });
        }

        private void stopwatch()
        {
            this.speak("I'll let you know when I'm back");
            System.Threading.Thread.Sleep(40000); {
                this.speak("I'm back.");
                this.end();
            };
        }
        private void timeout() {
            this.speak("Ok. See you in a few.");
            System.Threading.Thread.Sleep(59000); {
                this.speak("I hope that helped.");
                this.end();
            };
        }
        private void reboot() {
            this.speakRandom_ListenForAcceptance(new string[] {
                "Restart your system now?",
                "You'll probably lose anything open that hasn't been saved - are you sure?"
            }, delegate (bool accepted) {
                if (accepted) {
                    System.Diagnostics.Process.Start("Shutdown", "/r /t 0");
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                    this.end();
                } else {
                    this.speakCategory("okay");
                    this.end();
                }
            }, "Have you decided on wether or not to restart your computer");
        }
        private void punchout() {
            this.speakRandom_ListenForAcceptance(new string[] {
                "Are you certain you don't need me any more?",
                "Shall I be off then?"
            }, delegate (bool accepted) { 
                if (accepted) {
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                        } else {
                    this.speak("I knew you liked having me around.");
                    this.end();          
                }
            }, "I am waiting for you to decide if I should shut myself down or not.");
        }
        private void clockout() {
            this.speakRandom_ListenForAcceptance(new string[] {
                "Immediately?",
                "Initiate Omega protocol?"
            }, delegate (bool accepted) {
                if (accepted) {
                    System.Diagnostics.Process.Start("Shutdown", "/s t o");
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                } else {
                    this.speak("A bit dramatic, don't you think?");
                    this.end();
                }
            }, "I am waiting for your decision on the omega protocol");
        }
        public override void update() {
        }
        public override string getDescription(string phrase) {
            switch (phrase) {
                case "stopwatch":
                    return "This command is for when you really need two minutes of silence.";
                case "timeout":
                    return "This command gives me a five minute time out.";
                case "reboot":
                    return "This command restarts your computer when executed.";
                case "punchout":
                    return "This command shuts down my program.";
                case "clockout":
                    return "This command shuts down the entire system immediately upon execution.";
            }
            return "This command is used to pause, restart, or shut down systems - either mine or yours.";
        }
    }
}
