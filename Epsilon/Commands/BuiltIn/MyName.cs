﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class MyName : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "what is my name",
                "what's my name",
                "tell me my name",
                "set my name",
                "get my name"
            };
        }

        public override void start(string phrase) {
            string name = this.getUserName();
            string speak = "";
            if(name.Length > 0) {
                this.speakRandom(new string[] {
                    "Your name is " + name + "of course. Do you want to change it?",
                    "I like to call you " + name + ". Do you want me to call you something else?",
                    "You told me to call you " + name + ". Do you want me to call you something else?"
                });
            } else {
                this.speakRandom(new string[] {
                    "This is awkward. I don't actually know your name.",
                    "You haven't actually told me your name.",
                    "I actually don't know what your name is!"
                });
                this.speak("Do you want to tell me your name?");
            }

            this.speak_ListenForAcceptance(speak, delegate (bool acceptance) {
                if (acceptance) {
                    this.changeName();
                } else {
                    this.speakCategory("okay");
                    this.end();
                }
            });
        }

        private void changeName() {
            this.speak_ListenForAnything("What is your name?", delegate (string text) {
                this.confirmPhrase_ListenForAcceptance(text, delegate (bool acceptance) {
                    if (acceptance) {
                        this.speakCategory("okay");
                        this.speak("From now on I will call you " + text);
                        this.setUserName(text);
                        this.end();
                    } else {
                        this.changeName();
                    }
                });
            }, "I am waiting for you to tell me your name.");
        }

        public override void update() {

        }

        public override string getDescription(string phrase) {
            return "This command lets you set your name. You can initiate this command in a number of ways. For example, try saying: what's my name? or: tell me my name.";
        }
    }
}
