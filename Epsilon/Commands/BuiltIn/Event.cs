﻿using Epsilon.Commons;
using Epsilon.Enums;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class Event : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "create event",
                "delete event",
                "list events"
            };
        }

        public override string getDescription(string phrase) {
            switch (phrase) {
                case "create event":
                    return "This command is used to create a new event.";
                case "delete event":
                    return "This command is used to delete an extisting event.";
                case "list events":
                    return "This command is used to list all current events.";
            }
            return "Use this command to manage stored events.";
        }

        private DateTime nextCheck = DateTime.Now;

        public override void update() {
            //if we need to check for new events
            if (DateTime.Now >= this.nextCheck) {
                //check again in a minute
                this.nextCheck.AddSeconds(60);
                //get events
                List<Objects.Event> dues = this.getEventsDueData();
                foreach (Objects.Event @event in dues) {
                    //create a command event that expires in 30 mins
                    this.addCommandEvent("event_event_" + @event.id, delegate () {
                        @event.lastUsed = DateTime.Now.ToString("yyyy-MM-dd");
                        this.updateEventData(@event);
                        switch (@event.type) {
                            case EventTypes.text:
                                this.speak(@event.content);
                                this.endToIdle();
                                break;
                            case EventTypes.recording:
                                PromptBuilder pb = new PromptBuilder();
                                pb.AppendAudio(@event.content);
                                this.speak(pb);
                                this.endToIdle();
                                break;
                            case EventTypes.command:
                                this.endAndstartCommand(@event.content);
                                break;
                        }
                    }, 0, null, DateTime.Now.AddMinutes(30));
                }
            }
        }

        public override void start(string phrase) {
            switch (phrase) {
                case "create event":
                    this.createEvent();
                    break;
                case "delete event":
                    this.deleteEvent();
                    break;
                case "list events":
                    this.listEvents();
                    break;
            }
        }

        private Objects.Event @event;

        private void createEvent() {
            this.speak_ListenForAnything("Please give your event a name.", delegate (string name) {
                this.confirmPhrase_ListenForAcceptance(name, delegate (bool accept) {
                    if (accept) {
                        if (this.eventExistsData(name)) {
                            this.speak_ListenForAcceptance("An event with the name [" + name + "] already exists. Do you want to try again?", delegate (bool yes) {
                                if (yes) {
                                    this.createEvent();
                                } else {
                                    this.speakCategory("okay");
                                    this.end();
                                }
                            });
                        } else {
                            this.@event = new Objects.Event();
                            this.@event.name = name;
                            this.setPeriod();
                        }
                    } else {
                        this.createEvent();
                    }
                });
            }, "I am waiting for you to give me the name of your new event.");
        }

        private void setPeriod() {
            this.speak_ListenFor("Please state the period for your event.", new string[] {
                EventPeriods.specific_day,
                EventPeriods.every_day,
                EventPeriods.weekend_days,
                EventPeriods.week_days,
                EventPeriods.mondays,
                EventPeriods.tuesdays,
                EventPeriods.wednesdays,
                EventPeriods.thursdays,
                EventPeriods.fridays,
                EventPeriods.saturdays,
                EventPeriods.sundays
            }, delegate (string text) {
                this.@event.period = text;
                if (text == EventPeriods.specific_day) {
                    this.setMonth();
                } else {
                    this.setHour();
                }
            }, "I am waiting for an event period. You can say: specific day, every day, week days, weekend days, mondays, tuesdays, wednesdays, thursdays, fridays, saturdays or sundays.");
        }

        private void setMonth() {
            this.speak_ListenForNumberRange("Please say the month in numerical format.", 1, 12, delegate (int num) {
                this.@event.date = num.ToString();
                this.setDay();
            });
        }

        private void setDay() {
            this.speak_ListenForNumberRange("Please say the day of the month in numerical format.", 1, 31, delegate (int num) {
                this.@event.date += "-" + num.ToString();
                this.setHour();
            });
        }

        private void setHour() {
            this.speak_ListenForNumberRange("Please say the hour in 24 hour format.", 0, 23, delegate (int num) {
                this.@event.time = num.ToString();
                this.setMinute();
            });
        }

        private void setMinute() {
            this.speak_ListenForNumberRange("Please say the minutes within the hour.", 0, 59, delegate (int num) {
                this.@event.time += ":" + num.ToString();
                this.setType();
            });
        }

        private void setType() {
            this.speak_ListenFor("Please state the event type.", new string[] {
                EventTypes.recording,
                EventTypes.text,
                EventTypes.command
            }, delegate (string text) {
                this.@event.type = text;
                switch (text) {
                    case EventTypes.recording:
                        this.setTypeRecording();
                        break;
                    case EventTypes.text:
                        this.setTypeText();
                        break;
                    case EventTypes.command:
                        this.setTypeCommand();
                        break;
                }
            }, "I am waiting for an event type. You can say: recording, text or command.");
        }

        private void setTypeRecording() {
            this.speak("Recording in 3...2...1");
            this.recordAnything(delegate (RecognizedAudio audio) {
                string filePath = Core.instance.projectRoot + "Assets\\Audio\\event_" + this.@event.name + ".wav";
                if (File.Exists(filePath)) {
                    File.Delete(filePath);
                }
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate);
                audio.WriteToWaveStream(fs);
                fs.Close();
                this.speak("I got: ");
                PromptBuilder pb = new PromptBuilder();
                pb.AppendAudio(filePath);
                this.speak(pb);
                this.speak_ListenForAcceptance("Are you happy with this recording?", delegate (bool accepted) {
                    if (accepted) {
                        this.@event.content = filePath;
                        this.createEventData(this.@event);
                        this.speak("Your event was successfully created.");
                        this.end();
                    } else {
                        File.Delete(filePath);
                        this.setTypeRecording();
                    }
                });
            });
        }

        private void setTypeText() {
            this.speak_ListenForAnything("Please say the text content of this event.", delegate (string text) {
                this.confirmPhrase_ListenForAcceptance(text, delegate (bool accepted) {
                    if (accepted) {
                        this.@event.content = text;
                        this.createEventData(this.@event);
                        this.speak("Your event was successfully created.");
                        this.end();
                    } else {
                        this.setTypeText();
                    }
                });
            }, null, false);
        }

        private void setTypeCommand() {
            this.speak_ListenForCommandPhrase("Please say a command to run for this event.", delegate (string text) {
                this.confirmPhrase_ListenForAcceptance(text, delegate (bool accept) {
                    if (accept) {
                        this.@event.content = text;
                        this.createEventData(this.@event);
                        this.speak("Your event was successfully created.");
                        this.end();
                    } else {
                        this.setTypeCommand();
                    }
                });
            });
        }

        private void deleteEvent() {
            this.speak_listenForDataList("Please say the name of the event.", new Helpers.DataList("Event", "name", 1000, "id DESC"), delegate (string text) {
                this.speak_ListenForAcceptance("Are you sure you want to delete this event?", delegate (bool accept) {
                    if (accept) {
                        this.deleteEventData(text);
                        this.speak("Event successfully deleted.");
                        this.end();
                    } else {
                        this.speakCategory("okay");
                        this.end();
                    }
                });
            }, "I am waiting for you to say the name of the event that you want to delete.");
        }

        private void listEvents() {
            this.speak("The currently stored events are: ");
            this.speakDataList(new Helpers.DataList("Event", "name", 8, "id DESC"), delegate () {
                this.speak("I have finished listing the stored events.");
                this.end();
            });
        }

        private List<Objects.Event> getEventsDueData() {
            DateTime now = DateTime.Now;
            string weekDay = "";
            string weekPart = "";
            //work out whether day of week is week day or week end
            switch (now.DayOfWeek.ToString()) {
                case "Monday":
                    weekDay = EventPeriods.mondays;
                    weekPart = EventPeriods.week_days;
                    break;
                case "Tuesday":
                    weekDay = EventPeriods.tuesdays;
                    weekPart = EventPeriods.week_days;
                    break;
                case "Wednesday":
                    weekDay = EventPeriods.wednesdays;
                    weekPart = EventPeriods.week_days;
                    break;
                case "Thursday":
                    weekDay = EventPeriods.thursdays;
                    weekPart = EventPeriods.week_days;
                    break;
                case "Friday":
                    weekDay = EventPeriods.fridays;
                    weekPart = EventPeriods.week_days;
                    break;
                case "Saturday":
                    weekDay = EventPeriods.saturdays;
                    weekPart = EventPeriods.weekend_days;
                    break;
                case "Sunday":
                    weekDay = EventPeriods.sundays;
                    weekPart = EventPeriods.weekend_days;
                    break;
            }

            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT * FROM Event WHERE (lastUsed IS NULL OR lastUsed < @1) AND time <= @2 AND (period = @3 OR period = @4 OR period = @5 OR (period = @6 AND date = @7))";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", now.ToString("yyyy-MM-dd")));
            c.Parameters.Add(new SQLiteParameter("@2", now.ToString("H:m")));
            c.Parameters.Add(new SQLiteParameter("@3", EventPeriods.every_day));
            c.Parameters.Add(new SQLiteParameter("@4", weekDay));
            c.Parameters.Add(new SQLiteParameter("@5", weekPart));
            c.Parameters.Add(new SQLiteParameter("@6", EventPeriods.specific_day));
            c.Parameters.Add(new SQLiteParameter("@7", now.ToString("M-d")));
            SQLiteDataReader r = c.ExecuteReader();
            List<Objects.Event> events = new List<Objects.Event>();
            while (r.Read()) {
                events.Add(this.constructFromReader(r));
            }
            return events;
        }

        private bool eventExistsData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT id FROM Event WHERE name = @1 LIMIT 1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            SQLiteDataReader r = c.ExecuteReader();
            while (r.Read()) {
                return true;
            }
            return false;
        }

        private Objects.Event getEventData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT * FROM Event WHERE name = @1 LIMIT 1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            SQLiteDataReader r = c.ExecuteReader();
            while (r.Read()) {
                return this.constructFromReader(r);
            }
            return null;
        }

        private void createEventData(Objects.Event @event) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "INSERT INTO Event (name, period, time, type, content, lastUsed, date) VALUES (@1,@2,@3,@4,@5,'',@6)";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", @event.name));
            c.Parameters.Add(new SQLiteParameter("@2", @event.period));
            c.Parameters.Add(new SQLiteParameter("@3", @event.time));
            c.Parameters.Add(new SQLiteParameter("@4", @event.type));
            c.Parameters.Add(new SQLiteParameter("@5", @event.content));
            c.Parameters.Add(new SQLiteParameter("@6", @event.date != null ? @event.date : ""));
            c.ExecuteNonQuery();
        }

        private void updateEventData(Objects.Event @event) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "UPDATE Event SET name = @1, period = @2, time = @3, type = @4, content = @5, lastUsed = @6, date = @7 WHERE id = @8";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", @event.name));
            c.Parameters.Add(new SQLiteParameter("@2", @event.period));
            c.Parameters.Add(new SQLiteParameter("@3", @event.time));
            c.Parameters.Add(new SQLiteParameter("@4", @event.type));
            c.Parameters.Add(new SQLiteParameter("@5", @event.content));
            c.Parameters.Add(new SQLiteParameter("@6", @event.lastUsed != null ? @event.lastUsed : ""));
            c.Parameters.Add(new SQLiteParameter("@7", @event.date != null ? @event.date : ""));
            c.Parameters.Add(new SQLiteParameter("@8", @event.id));
            c.ExecuteNonQuery();
        }

        private void deleteEventData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "DELETE FROM Event WHERE name = @1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            c.ExecuteNonQuery();
        }

        private Objects.Event constructFromReader(SQLiteDataReader reader) {
            Objects.Event @event = new Objects.Event();
            @event.id = Convert.ToInt32(reader["id"]);
            @event.name = (string)reader["name"];
            @event.period = (string)reader["period"];
            @event.time = (string)reader["time"];
            @event.type = (string)reader["type"];
            @event.content = (string)reader["content"];
            @event.lastUsed = (string)reader["lastUsed"];
            @event.date = (string)reader["date"];          
            return @event;
        }
    }
}
