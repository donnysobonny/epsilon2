﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Epsilon.Commands.BuiltIn {
    public class OpenMy : CommandBase {
        public override void awake() {

        } public override string[] getCommandPhrases() {
            return new string[] {
                "open my hotmail",
                "open my gmail",
                "open my td account",
                "open my paypal",
                "open my CIBC",
                "open my udemy",
                "open my bitbucket",
                "open my rackspace",
                "open my yahoo",
                "open my twitter",
                "open my pintrest",
                "open my instagram"
            };
        }

        public override void start(string phrase) {
            switch (phrase) {
                case "open my hotmail":
                    System.Diagnostics.Process.Start("http://hotmail.com");
                    break;
                case "open my gmail":
                    System.Diagnostics.Process.Start("http://gmail.com");
                    break;
                case "open td account":
                    System.Diagnostics.Process.Start("http://tdcanadatrust.com");
                    break;
                case "open my paypal":
                    System.Diagnostics.Process.Start("http://paypal.com");
                    break;
                case "open my CIBC":
                    System.Diagnostics.Process.Start("http://cibc.com");
                    break;
                case "open my udemy":
                    System.Diagnostics.Process.Start("http://udemy.com");
                    break;
                case "open my rackspace":
                    System.Diagnostics.Process.Start("http://apps.rackspace.com");
                    break;
                case "open my yahoo":
                    System.Diagnostics.Process.Start("http://yahoo.com");
                    break;
                case "open my twitter":
                    System.Diagnostics.Process.Start("http://twitter.com");
                    break;
                case "open my pintrest":
                    System.Diagnostics.Process.Start("http://pintrest.com");
                    break;
                case "open my instagram":
                    System.Diagnostics.Process.Start("http://instagram.com");
                    break;

            }
            this.end();
        }
        public override string getDescription(string phrase) {
            return "This command lets me open up web pages for you. Just tell me which page to open up, and I'll bring it up right away.";
        }

        public override void update() {
 
        }
    }
}


