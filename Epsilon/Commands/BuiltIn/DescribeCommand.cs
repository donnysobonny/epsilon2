﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class DescribeCommand : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "describe command"
            };
        }

        public override string getDescription(string phrase) {
            return "This command is used to describe other commands.";
        }

        public override void start(string phrase) {
            Core core = Core.instance;
            this.speakRandom_ListenFor(new string[] {
                "Please state the command you want to describe.",
                "Which command do you want to describe?",
                "Which command would you like me to describe?"
            }, core.cachedCommandPhrases.ToArray(), delegate(string text) {
                if (core.cachedCommandPhrases.Contains(text)) {
                    this.speak(core.cachedCommands[core.cachedCommandPhrasesToCommands[text]].getDescription(text));
                    this.end();
                }
            }, "I am waiting for you to tell me the command that you want me to describe.");
        }

        public override void update() {

        }
    }
}
