﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class Testing : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "testing"
            };
        }

        public override string getDescription(string phrase) {
            return "This command is used for testing purposes";
        }

        public override void start(string phrase) {
            this.speak_ListenForAcceptance(
                "listening",
                delegate(bool accept) {
                    this.speak("accpeted!");
                    this.end();
                },
                "This is some help!",
                true,
                TimeSpan.FromSeconds(1),
                delegate() {
                    this.speak("timed out!");
                    this.end();
                }
            );
        }

        public override void update() {

        }
    }
}
