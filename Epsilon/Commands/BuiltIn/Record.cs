﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using System.Speech.Recognition;
using System.Speech.Synthesis;

namespace Epsilon.Commands.BuiltIn {
    public class Record : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "create recording",
                "play recording",
                "delete recording",
                "list recordings"
            };
        }

        public override string getDescription(string phrase) {
            switch (phrase) {
                case "create recording":
                    return "Use this command to create a new recording.";
                case "play recording":
                    return "Use this command to play back an existing recording.";
                case "delete recording":
                    return "Use this command to delete an existing recording.";
                case "list recordings":
                    return "Use this command to get a list of all recordings";
            }
            return "Use this command to create, play back and delete recordings";
        }

        public override void start(string phrase) {
            switch (phrase) {
                case "create recording":
                    this.createRecording();
                    break;
                case "play recording":
                    this.playRecording();
                    break;
                case "delete recording":
                    this.deleteRecording();
                    break;
                case "list recordings":
                    this.listRecordings();
                    break;
            }
        }

        public override void update() {

        }

        private void listRecordings() {
            this.speak("The currently stored recordings are: ");
            this.speakDataList(new Helpers.DataList("Recording", "name", 8, "id DESC"), delegate () {
                this.speak("I have finished listing the stored recordings.");
                this.end();
            });
        }

        private void createRecording() {
            this.speak_ListenForAnything("Please give your recording a name.", delegate (string name) {
                this.confirmPhrase_ListenForAcceptance(name, delegate (bool acceptance) {
                    if (acceptance) {
                        if (this.recordingExistsData(name)) {
                            this.speak_ListenForAcceptance("A recording already exists with this name. Do you want to try again?", delegate (bool accepted) {
                                if (accepted) {
                                    this.createRecording();
                                } else {
                                    this.speakCategory("okay");
                                    this.end();
                                }
                            });
                        } else {
                            this.createRecordingData(name);
                            this.speak("Starting recording in 3... 2.. 1");
                            this.recordAnything(delegate (RecognizedAudio audio) {
                                string path = Directory.GetCurrentDirectory() + "\\..\\..\\Assets\\Audio\\" + name + ".wav";
                                FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
                                audio.WriteToWaveStream(fs);
                                fs.Close();
                                this.setFilePathData(name, path);
                                this.speak("Recording successfully created.");
                                this.end();
                            });
                        }
                    } else {
                        this.createRecording();
                    }
                });
            }, "I am waiting for you to give me the name of your new recording.");
        }

        private void playRecording() {
            this.speak_listenForDataList("Please say the name of a recording.", new Helpers.DataList("Recording", "name", 1000, "id DESC"), delegate (string name) {
                if (this.recordingExistsData(name)) {
                    string filePath = this.getFilePathData(name);
                    if (File.Exists(filePath)) {
                        PromptBuilder pb = new PromptBuilder();
                        pb.AppendAudio(this.getFilePathData(name));
                        this.speak(pb);
                        this.end();
                    } else {
                        this.speak_ListenForAcceptance("I could not find a recording with the name " + name + ". Do you want to try again?", delegate (bool acceptance) {
                            if (acceptance) {
                                this.playRecording();
                            } else {
                                this.speakCategory("okay");
                                this.end();
                            }
                        });
                    }
                } else {
                    this.speak_ListenForAcceptance("I could not find a recording with the name " + name + ". Do you want to try again?", delegate (bool acceptance) {
                        if (acceptance) {
                            this.playRecording();
                        } else {
                            this.speakCategory("okay");
                            this.end();
                        }
                    });
                }
            }, "I am waiting for you to give me the name of a recording that you want to play.");
        }

        private void deleteRecording() {
            this.speak_listenForDataList("Please say the name of a recording.", new Helpers.DataList("Recording", "name", 1000, "id DESC"), delegate (string name) {
                if (this.recordingExistsData(name)) {
                    this.speak_ListenForAcceptance("Are you sure that you want to delete this recording?", delegate (bool accepted) {
                        if (accepted) {
                            this.deleteRecordingData(name);
                            this.speak("Recording successfully deleted");
                            this.end();
                        } else {
                            this.end();
                        }
                    });
                } else {
                    this.speak_ListenForAcceptance("I could not find a recording with the name " + name + ". Do you want to try again?", delegate (bool acceptance) {
                        if (acceptance) {
                            this.deleteRecording();
                        } else {
                            this.speakCategory("okay");
                            this.end();
                        }
                    });
                }
            }, "I am waiting for you to give me the name of a recording that you want to delete.");
        }

        private bool recordingExistsData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT id FROM Recording WHERE name = @1 LIMIT 1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            SQLiteDataReader r = c.ExecuteReader();
            while (r.Read()) {
                return true;
            }
            return false;
        }

        private void createRecordingData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "INSERT INTO Recording (name,filePath) VALUES (@1,'')";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            c.ExecuteNonQuery();
        }

        private string getFilePathData(string name) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT filePath FROM Recording WHERE name = @1 LIMIT 1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            SQLiteDataReader r = c.ExecuteReader();
            while (r.Read()) {
                return (string)r["filePath"];
            }
            return "";
        }

        private void setFilePathData(string name, string filePath) {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "UPDATE Recording SET filePath = @1 WHERE name = @2";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", filePath));
            c.Parameters.Add(new SQLiteParameter("@2", name));
            c.ExecuteNonQuery();

            new SQLiteCommand("UPDATE Recording SET filePath = ('" + filePath + "') WHERE name = ('" + name + "')", Core.instance.database).ExecuteNonQuery();
        }

        private void deleteRecordingData(string name) {
            string filePath = this.getFilePathData(name);
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "DELETE FROM Recording WHERE name = @1";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", name));
            c.ExecuteNonQuery();
            if (File.Exists(filePath)) {
                File.Delete(filePath);
            }
        }
    }
}
