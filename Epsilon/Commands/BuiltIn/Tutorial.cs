﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn {
    public class Tutorial : CommandBase {
        public override void awake() {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "tutorial"
            };
        }

        public override string getDescription(string phrase) {
            return "This command is used when you first run epsilon.";
        }

        public override void start(string phrase) {
            if (this.getFirstTimeRunning()) {
                this.speak("Hello. My name is " + this.getComputerName() + ", and I am a highly advanced program that can be used to serve many purposes.");
                this.speak_ListenForAcceptance("Would you like me to take you through a basic tutorial?", delegate (bool accept) {
                    if (accept) {
                        this.speakCategory("okay");
                        this.speak("Now that you know that my name is " + this.getComputerName() + ",");
                        this.begin();
                    } else {
                        this.speakOkayCategory();
                        this.end();
                    }
                }, null, false);
            } else {
                this.speak("We have already gone through the tutorial.");
                this.end();
            }
        }

        private void startFromReminder() {
            this.speakGreatCategory();
            this.speak("So, you probably already know that my name is " + this.getComputerName() + ", ");
            this.begin();
        }

        private void begin() {
            this.speak_ListenForAnything("please tell me your name.", delegate (string name) {
                this.confirmPhrase_ListenForAcceptance(name, delegate (bool accept) {
                    if (accept) {
                        this.speak(name + ". What a lovely name! From now on, I will call you " + name + ".");
                        this.setUserName(name);
                        this.speak("Next, we will talk about idle mode.");
                        this.speak("Whenever I am not doing something, I am in idle mode. While I am idle, to get my attention simply say my name.");
                        this.trainIdle();
                    } else {
                        this.begin();
                    }
                }, false);
            }, "I am waiting for you to tell me your name.", false);
        }

        private void trainIdle() {
            this.speak_ListenFor("Try saying my name now.", new string[] {
                this.getComputerName()
            }, delegate (string text) {
                this.speakGreatCategory();
                this.speak("While I am in idle mode, I am able to give you notifications such as reminders and so on.");
                this.speak("Next, we will talk about the help command.");
                this.speak("Whenever you are unsure what to do next, simply say: help. When you do this, I will give you more information about what it is that I am waiting for.");
                this.trainHelp();
            }, "You haven't forgotten my name already have you? My name is " + this.getComputerName() + ".", false);
        }

        private void trainHelp() {
            this.speak_ListenFor("Try saying help now.", new string[] { "help" }, delegate (string text) {
                this.speakGreatCategory();
                this.speak("The help command can be very useful whenever you get stuck. Not all commands have additional information though, so some times when you say help I wont be able to respond with anything.");
                this.speak("Lastly, we will talk about the cancel command.");
                this.speak("Whenever you want to cancel what ever it is that I am currently doing for you, simply say: cancel. When you do this, I will stop what it is that I am doing, and return to idle mode.");
                this.trainCancel();
            }, null, false);
        }

        private void trainCancel() {
            this.speak_ListenFor("Try saying cancel now.", new string[] { "cancel" }, delegate (string text) {
                this.speakGreatCategory();
                this.speak("It's worth noting that not all commands can be cancelled, so sometimes when you say cancel I will not be able to cancel the current operation.");
                this.speak("Remember that when I successfully cancel a command, I return to idle mode.");
                this.complete();
            }, "I am waiting for you to say cancel.", false);
        }

        private void complete() {
            this.speak("And that concludes the tutorial! You did very well!");
            this.speak("I am going into idle mode now. If you need me, remember to simply say my name. Goodbye for now, " + this.getUserName() + ".");

            this.setFirstTimeRunning(false);

            this.endToIdle();
        }

        private void reminder() {
            this.speakRandom_ListenForAcceptance(new string[] {
                "Sorry to bother you {0}, but I would really like to take you through the tutorial. Can we do it now?",
                "Hi {0}. I see that you have not yet completed the tutorial. Would you like to go through it now?",
                "I just wanted to let you know that I have not yet gone through the tutorial with you. Would you like to do it now?"
            }, delegate (bool approve) {
                if(approve) {
                    this.startFromReminder();
                } else {
                    this.speakWhateverCategory();
                    this.endToIdle();
                }
            }, "I am waiting for you to tell me if you want to go through the tutorial.");
        }

        public override void update() {
            if(this.getFirstTimeRunning() && !this.commandEventExists("tutorial_reminder")) {
                this.addCommandEvent(new Helpers.CommandEvent("tutorial_reminder", delegate () {
                    this.reminder();
                }, 0, DateTime.Now.AddMinutes(5)));
            }
        }
    }
}
