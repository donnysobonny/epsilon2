﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn
{
    public class Google : CommandBase
    {
        public override void awake()
        {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "google something"
            };
        }

        public override void start(string phrase) {
            this.search();
        }

        private void search() {
            this.speakRandom_ListenForAnything(new string[] {
                "what should I look for?",
                "Certainly. What do you need me to goole?",
                "What do you need me to search?"
            }, delegate (string text) {

                this.confirmPhrase_ListenForAcceptance(text, delegate (bool accepted) {
                    if (accepted) {
                        System.Diagnostics.Process.Start("http://google.com?q=" + WebUtility.UrlEncode(text));
                        this.speak("Searching for: " + text + ".");
                        this.speakRandom_ListenForAcceptance(new string[] {
                            "Did you want to search again?",
                            "Did you want to search for something different?",
                            "Anything else you want to search for?"
                        }, delegate (bool acceptance) {
                            if (acceptance) {
                                this.search();
                            } else {
                                this.speakCategory("okay");
                                this.end();
                            }
                        }, "I'm still waiting for you to tell me if you need me to search again.");
                    } else {
                        this.search();
                    }
                });
            }, "I am waiting for you to tell me what to google.");
        }

        public override void update() {

        }

        public override string getDescription(string phrase) {
            return "This command allows you to search google. To initiate this command, you can say: google search.";
        }
    }
}
