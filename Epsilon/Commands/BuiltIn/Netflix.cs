﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Commands.BuiltIn
{
    public class Netflix : CommandBase
    {
        public override void awake()
        {

        }

        public override string[] getCommandPhrases() {
            return new string[] {
                "Watch Netflix"
            };
        }

        public override void start(string phrase) {
            this.search();
        }

        private void search() {
            this.speakRandom_ListenForAnything(new string[] {
                "What should I put on?",
                "What should I search for?",
                "Is there a genre or title I can find for you?"
            }, delegate (string text) {

                this.confirmPhrase_ListenForAcceptance(text, delegate (bool accepted) {
                    if (accepted) {
                        System.Diagnostics.Process.Start("http://netflix.com/search/" + WebUtility.UrlEncode(text));
                        this.speak("Searching for: " + text + ".");
                        this.speakRandom_ListenForAcceptance(new string[] {
                            "Did you want to search again?",
                            "Want me to look for something else?",
                            "Anything else I can search for?"
                        }, delegate (bool acceptance) {
                            if (acceptance) {
                                this.search();
                            } else {
                                this.speakCategory("okay");
                                this.end();
                            }
                        }, "I'm still waiting for you to tell me if you need me to search again.");
                    } else {
                        this.search();
                    }
                });
            }, "I am waiting for you to tell me what you wanted to watch.");
        }

        public override void update() {

        }

        public override string getDescription(string phrase) {
            return "This command allows you to search Netflix. To initiate this command, you can say: 'Watch netflix'.";
        }
    }
}
