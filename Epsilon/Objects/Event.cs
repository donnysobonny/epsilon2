﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Objects {
    public class Event {
        public int id;
        public string name;
        public string period;
        public string date;
        public string time;
        public string type;
        public string content;
        public string lastUsed;
    }
}