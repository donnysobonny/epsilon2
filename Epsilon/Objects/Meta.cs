﻿using LitJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Objects {
    public class Meta {
        public string computerName;
        public string userName;
        public bool firstTimeRunning;
    }
}
