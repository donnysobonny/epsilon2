﻿using Epsilon.Commons;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Helpers {
    public class DataList {

        private string table;
        private string field;
        private string where;
        private string orderBy;
        private int limit = 10;
        private int currentPage = 0;

        public DataList(string table, string field, int limit = 10, string orderBy = null, string where = null) {
            this.table = table;
            this.field = field;
            this.where = where;
            this.orderBy = orderBy;
            this.limit = limit;
        }

        public List<string> getList() {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT `" + this.field + "` FROM `" + this.table + "` ";
            if (this.where != null) {
                if (this.where.Length > 0) {
                    c.CommandText += "WHERE " + this.where + " ";
                }
            }
            if (this.orderBy != null) {
                if (this.orderBy.Length > 0) {
                    c.CommandText += "ORDER BY " + this.orderBy + " ";
                }
            }
            c.CommandText += "LIMIT @1, @2";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", this.limit * this.currentPage));
            c.Parameters.Add(new SQLiteParameter("@2", this.limit));
            SQLiteDataReader r = c.ExecuteReader();
            List<string> list = new List<string>();
            while (r.Read()) {
                list.Add(r[this.field].ToString());
            }
            return list;
        }

        public bool hasNextPage() {
            SQLiteCommand c = new SQLiteCommand();
            c.Connection = Core.instance.database;
            c.CommandText = "SELECT `" + this.field + "` FROM `" + this.table + "` ";
            if (this.where != null) {
                if (this.where.Length > 0) {
                    c.CommandText += "WHERE " + this.where + " ";
                }
            }
            if (this.orderBy != null) {
                if (this.orderBy.Length > 0) {
                    c.CommandText += "ORDER BY " + this.orderBy + " ";
                }
            }
            c.CommandText += "LIMIT @1, @2";
            c.CommandType = System.Data.CommandType.Text;
            c.Parameters.Add(new SQLiteParameter("@1", this.limit * (this.currentPage + 1)));
            c.Parameters.Add(new SQLiteParameter("@2", 1));
            SQLiteDataReader r = c.ExecuteReader();
            while (r.Read()) {
                return true;
            }
            return false;
        }

        public void nextPage() {
            this.currentPage++;
        }
    }
}
