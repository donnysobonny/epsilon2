﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epsilon.Helpers {
    public class CommandEvent {
        private string name;
        private int priority = 0;
        private DateTime? expireAfter;
        private DateTime? startAfter;
        private Action action;

        public CommandEvent(string name, Action action, int priority = 0, DateTime? startAfter = null, DateTime? expireAfter = null) {
            this.name = name;
            this.action = action;
            this.priority = priority;
            this.expireAfter = expireAfter;
            this.startAfter = startAfter;
        }

        public string getName() {
            return this.name;
        }

        public int getPriority() {
            return this.priority;
        }

        public DateTime? getExpiryAfter() {
            return this.expireAfter;
        }

        public DateTime? getStartAfter() {
            return this.startAfter;
        }

        public void run() {
            this.action();
        }
    }
}
