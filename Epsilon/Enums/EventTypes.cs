﻿namespace Epsilon.Enums {
    public static class EventTypes {
        public const string recording = "recording";
        public const string text = "text";
        public const string command = "command";
    }
}
