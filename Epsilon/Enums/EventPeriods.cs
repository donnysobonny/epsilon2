﻿namespace Epsilon.Enums {
    public static class EventPeriods {
        public const string specific_day = "specific day";
        public const string every_day = "every day";
        public const string week_days = "week days";
        public const string weekend_days = "weekend days";
        public const string mondays = "mondays";
        public const string tuesdays = "tuesdays";
        public const string wednesdays = "wednesdays";
        public const string thursdays = "thursdays";
        public const string fridays = "fridays";
        public const string saturdays = "saturdays";
        public const string sundays = "sundays";
    }
}
