﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Speech.Synthesis;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;
using Epsilon.Helpers;

namespace Epsilon.Commons {
    public abstract class CommandBase {
        public abstract string getDescription(string phrase);
        public abstract string[] getCommandPhrases();
        public abstract void awake();
        public abstract void start(string phrase);
        public abstract void update();

        /// <summary>
        /// Get whether epsilon has gone through the tutorial
        /// </summary>
        /// <returns></returns>
        protected bool getFirstTimeRunning() {
            return Core.instance.cachedMeta.firstTimeRunning;
        }

        /// <summary>
        /// Set whether epsilon has gone through the tutorial
        /// </summary>
        /// <param name="firstTimeRunning"></param>
        protected void setFirstTimeRunning(bool firstTimeRunning) {
            Core core = Core.instance;
            core.cachedMeta.firstTimeRunning = firstTimeRunning;
            core.saveMeta();
        }

        /// <summary>
        /// Get the computer name
        /// </summary>
        /// <returns>the computer's name</returns>
        protected string getComputerName() {
            return Core.instance.cachedMeta.computerName;
        }

        /// <summary>
        /// Set the computer name
        /// </summary>
        /// <param name="computerName">the computer's name</param>
        protected void setComputerName(string computerName) {
            Core core = Core.instance;
            core.cachedMeta.computerName = computerName;
            core.saveMeta();
        }

        /// <summary>
        /// Get the user's name
        /// </summary>
        /// <returns>the user's name</returns>
        protected string getUserName() {
            return Core.instance.cachedMeta.userName;
        }

        /// <summary>
        /// Set the user's name
        /// </summary>
        /// <param name="userName">the user's name</param>
        protected void setUserName(string userName) {
            Core core = Core.instance;
            core.cachedMeta.userName = userName;
            core.saveMeta();
        }

        /// <summary>
        /// Get speech phrases by category
        /// </summary>
        /// <param name="category">the category</param>
        /// <returns>a list of phrases</returns>
        protected List<string> getSpeechPhrasesByCategory(string category) {
            SQLiteDataReader r = new SQLiteCommand("SELECT phrase FROM SpeechPhrase WHERE category = ('" + category + "')", Core.instance.database).ExecuteReader();
            List<string> phrases = new List<string>();
            while (r.Read()) {
                phrases.Add((string)r["phrase"]);
            }
            return phrases;
        }

        /// <summary>
        /// Get listen phrases by category
        /// </summary>
        /// <param name="category">the category</param>
        /// <returns>a list of phrases</returns>
        protected List<string> getListenPhrasesByCategory(string category) {
            SQLiteDataReader r = new SQLiteCommand("SELECT phrase FROM ListenPhrase WHERE category = ('" + category + "')", Core.instance.database).ExecuteReader();
            List<string> phrases = new List<string>();
            while (r.Read()) {
                phrases.Add((string)r["phrase"]);
            }
            return phrases;
        }

        /// <summary>
        /// Record anything, and recognize the audio.
        /// 
        /// This can be used to make a recording of the user's speech.
        /// </summary>
        /// <param name="onRecognize">action passing in the recognized audio</param>
        protected void recordAnything(Action<RecognizedAudio> onRecognize) {
            Core.instance.recordAnything(onRecognize);
        }

        private void speakHelp_ListenFor(
            string speak,
            string[] listenFor,
            Action<string> onRecognize,
            string helpText = null,
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            //add the internal phrases
            List<string> repeat = this.getListenPhrasesByCategory("repeat");
            listenFor = listenFor.Union(repeat).ToArray();
            List<string> help = new List<string>();
            if (helpText != null) {
                help = this.getListenPhrasesByCategory("help");
                listenFor = listenFor.Union(help).ToArray();
            }
            List<string> cancel = new List<string>();
            if (canBeCancelled) {
                cancel = this.getListenPhrasesByCategory("cancel");
                listenFor = listenFor.Union(cancel).ToArray();
            }

            //speak
            this.speak(helpText);

            //listen
            Core.instance.listenFor(listenFor, delegate (string text) {
                //if repeating
                if (repeat.Contains(text)) {
                    if (speak.Length <= 0) {
                        this.speak("I have nothing to repeat.");
                    }
                    //start again
                    this.speak_ListenFor(speak, listenFor, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
                }
                //if help
                else if (helpText != null && help.Contains(text)) {
                    //speak help text
                    this.speakHelp_ListenFor(speak, listenFor, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
                }
                //if cancel
                else if (canBeCancelled && cancel.Contains(text)) {
                    //speak cancel
                    Core.instance.speak("Cancelled.");
                    //go to idle
                    this.idle();
                } else {
                    //recognize
                    onRecognize(text);
                }
            }, timeout, onTimeout);
        }

        private void speakHelp_ListenForAnything(
            string speak,
            Action<string> onRecognize,
            string helpText = null,
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            //speak
            this.speak(helpText);

            //listen
            Core.instance.listenForAnything(delegate (string text) {
                //if repeat
                if (this.getListenPhrasesByCategory("repeat").Contains(text)) {
                    if (speak.Length <= 0) {
                        this.speak("I have nothing to repeat.");
                    }
                    //try again
                    this.speak_ListenForAnything(speak, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
                }
                //if help
                else if (helpText != null && this.getListenPhrasesByCategory("help").Contains(text)) {
                    //speak help text
                    this.speakHelp_ListenForAnything(speak, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
                }
                //if cancel
                else if (canBeCancelled && this.getListenPhrasesByCategory("cancel").Contains(text)) {
                    //speak cancel
                    Core.instance.speak("Cancelled.");
                    //go to idle
                    this.idle();
                } else {
                    //recognize
                    onRecognize(text);
                }
            }, timeout, onTimeout);
        }

        /// <summary>
        /// Speak, and listen for one out of a set of phrases.
        /// </summary>
        /// <param name="speak">what to speak</param>
        /// <param name="listenFor">a list of phrases you are listening for</param>
        /// <param name="onRecognize">when one of the phrases are recognized, this action is triggered, passing in the recognized phrase</param>
        /// <param name="helpText">optional help text, said when the user says "help"</param>
        /// <param name="canBeCancelled">whether the command can be cancelled or not</param>
        protected void speak_ListenFor(
            string speak, 
            string[] listenFor, 
            Action<string> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true, 
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            //add the internal phrases
            List<string> repeat = this.getListenPhrasesByCategory("repeat");
            listenFor = listenFor.Union(repeat).ToArray();
            List<string> help = new List<string>();
            if (helpText != null) {
                help = this.getListenPhrasesByCategory("help");
                listenFor = listenFor.Union(help).ToArray();
            }
            List<string> cancel = new List<string>();
            if (canBeCancelled) {
                cancel = this.getListenPhrasesByCategory("cancel");
                listenFor = listenFor.Union(cancel).ToArray();
            }

            //speak
            this.speak(speak);

            //listen
            Core.instance.listenFor(listenFor, delegate (string text) {
                //if repeating
                if (repeat.Contains(text)) {
                    if (speak.Length <= 0) {
                        this.speak("I have nothing to repeat.");
                    }
                    //start again
                    this.speak_ListenFor(speak, listenFor, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
                }
                //if help
                else if (helpText != null && help.Contains(text)) {
                    //speak help text
                    this.speakHelp_ListenFor(speak, listenFor, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
                }
                //if cancel
                else if (canBeCancelled && cancel.Contains(text)) {
                    //speak cancel
                    Core.instance.speak("Cancelled.");
                    //go to idle
                    this.idle();
                } else {
                    //recognize
                    onRecognize(text);
                }
            }, timeout, onTimeout);
        }

        /// <summary>
        /// Speak, and listen for anything.
        /// </summary>
        /// <param name="speak">what to speak</param>
        /// <param name="onRecognize">when a phrase is recognized, this action is triggered, passing in the recognized phrase</param>
        /// <param name="helpText">optional help text, said when the user says "help"</param>
        /// <param name="canBeCancelled">whether the command can be cancelled or not</param>
        protected void speak_ListenForAnything(
            string speak, 
            Action<string> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true, 
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            //speak
            this.speak(speak);

            //listen
            Core.instance.listenForAnything(delegate (string text) {
                //if repeat
                if (this.getListenPhrasesByCategory("repeat").Contains(text)) {
                    if (speak.Length <= 0) {
                        this.speak("I have nothing to repeat.");
                    }
                    //try again
                    this.speak_ListenForAnything(speak, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
                }
                //if help
                else if (helpText != null && this.getListenPhrasesByCategory("help").Contains(text)) {
                    //speak help text
                    this.speakHelp_ListenForAnything(speak, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
                }
                //if cancel
                else if (canBeCancelled && this.getListenPhrasesByCategory("cancel").Contains(text)) {
                    //speak cancel
                    Core.instance.speak("Cancelled.");
                    //go to idle
                    this.idle();
                } else {
                    //recognize
                    onRecognize(text);
                }
            }, timeout, onTimeout);
        }

        /// <summary>
        /// Speak, and listen for acceptance (yes or no).
        /// </summary>
        /// <param name="speak">what to speak</param>
        /// <param name="onRecognize">when yes or no is recognized, this action is triggered, passing in true (yes) or false (no)</param>
        /// <param name="helpText">optional help text, said when the user says "help"</param>
        /// <param name="canBeCancelled">whether the command can be cancelled or not</param>
        protected void speak_ListenForAcceptance(
            string speak, 
            Action<bool> onRecognize, 
            string helpText = "I am waiting for you to say yes or no.", 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            List<string> yes = this.getListenPhrasesByCategory("yes");
            List<string> no = this.getListenPhrasesByCategory("no");
            List<string> both = new List<string>();
            foreach (string s in yes) {
                both.Add(s);
            }
            foreach (string s in no) {
                both.Add(s);
            }
            this.speak_ListenFor(speak, both.ToArray(), delegate (string text) {
                if (yes.Contains(text)) {
                    onRecognize(true);
                } else if (no.Contains(text)) {
                    onRecognize(false);
                } else {
                    onRecognize(false);
                }
            }, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak, and listen for a data list.
        /// 
        /// This allows you to use a DataList object to specify a list of phrases that you are looking for. For example, listen for the "name" values of notes.
        /// </summary>
        /// <param name="speak">what to speak</param>
        /// <param name="dataList">A DataList instance</param>
        /// <param name="onRecognize">When a phrase from the data list is recognized, this action is triggered, passing in the phrase</param>
        /// <param name="helpText">optional help text, said when the user says "help"</param>
        /// <param name="canBeCancelled">whether the command can be cancelled or not</param>
        protected void speak_listenForDataList(
            string speak, 
            DataList dataList, 
            Action<string> onRecognize, 
            string helpText = null,
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speak_ListenFor(speak, dataList.getList().ToArray(), onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak and listen for a number between a number range.
        /// 
        /// Note that there is no "helpText" argument to this method. Instead, when "help" is initiated, it will say:
        /// "I am waiting for a number between from and to"
        /// </summary>
        /// <param name="speak">what to speak</param>
        /// <param name="from">the smaller number of the range</param>
        /// <param name="to">the larger number of the range</param>
        /// <param name="onRecognize">when a number within the range is recognized, this action is triggered, passing in the number</param>
        /// <param name="canBeCancelled">whether the command can be cancelled or not</param>
        protected void speak_ListenForNumberRange(
            string speak, 
            int from, 
            int to, 
            Action<int> onRecognize, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            List<string> numbers = new List<string>();
            for (int i = from; i <= to; i++) {
                numbers.Add(i.ToString());
            }
            this.speak_ListenFor(speak, numbers.ToArray(), delegate (string text) {
                onRecognize(Convert.ToInt32(text));
            }, "I am waiting for a number between " + from + " and " + to + ".", canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak and listen for a command phrase
        /// </summary>
        /// <param name="speak">what to speak</param>
        /// <param name="onRecognize">when a command phrase is recognized, this event is triggered passing in the command phrase</param>
        /// <param name="helpText">optiona help text</param>
        /// <param name="canBeCancelled">whether the command can be cancelled or not</param>
        protected void speak_ListenForCommandPhrase(
            string speak, 
            Action<string> onRecognize, 
            string helpText = "I am waiting for you to tell me a command.", 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speak_ListenFor(speak, Core.instance.cachedCommandPhrases.ToArray(), onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of phrases, and listen for one out of a set of phrases.
        /// </summary>
        /// <param name="speak">a list of speech phrases. One will be chosen at random and spoken</param>
        /// <param name="listenFor">a list of phrases to listen for</param>
        /// <param name="onRecognize">when one of the phrases is recognized, this action is triggered, passing in the phrase</param>
        /// <param name="helpText">optional help text, to be said when the user says "help"</param>
        /// <param name="canBeCancelled">whether the command can be cancelled or not</param>
        protected void speakRandom_ListenFor(
            string[] speak, 
            string[] listenFor, 
            Action<string> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            Random r = new Random();
            this.speak_ListenFor(speak[r.Next(speak.Length)], listenFor, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of phrases, and listen for anything
        /// </summary>
        /// <param name="speak">a list of speech phrases. One will be chosen at random and spoken</param>
        /// <param name="onRecognize">when a phrase is recognized, this action is triggered, passing in the phrase</param>
        /// <param name="helpText">optional help text to be spoken when the user says "help"</param>
        /// <param name="canBeCancelled">whether this command can be cancelled or not</param>
        protected void speakRandom_ListenForAnything(
            string[] speak, 
            Action<string> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            Random r = new Random();
            this.speak_ListenForAnything(speak[r.Next(speak.Length)], onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of phrases, and listen for acceptance (yes or no)
        /// </summary>
        /// <param name="speak">a list of speech phrases. One will be chosen at random and spoken</param>
        /// <param name="onRecognize">when yes or no is recognized, this action is triggered, passing in true (yes) or false (no)</param>
        /// <param name="helpText">optional help text to be spoken when the user says "help"</param>
        /// <param name="canBeCancelled">whether this command can be cancelled or not</param>
        protected void speakRandom_ListenForAcceptance(
            string[] speak, 
            Action<bool> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            Random r = new Random();
            this.speak_ListenForAcceptance(speak[r.Next(speak.Length)], onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of phrases, and listen for a data list.
        /// 
        /// This allows you to use a DataList object to specify a list of phrases that you are looking for. For example, listen for the "name" values of notes.
        /// </summary>
        /// <param name="speak">a list of speech phrases. One will be chosen at random and spoken</param>
        /// <param name="dataList">A DataList instance</param>
        /// <param name="onRecognize">When a phrase from the list is recognized, this action is triggered, passing in the phrase</param>
        /// <param name="helpText">optional help text, to be spoken when the user says "help"</param>
        /// <param name="canBeCancelled">whether this command can be cancelled or not</param>
        protected void speakRandom_ListenForDataList(
            string[] speak, 
            DataList dataList, 
            Action<string> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speakRandom_ListenFor(speak, dataList.getList().ToArray(), onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of phrases, and listen for a number between a number range.
        /// 
        /// Note that there is no "helpText" argument to this method. Instead, when "help" is initiated, it will say:
        /// "I am waiting for a number between from and to"
        /// </summary>
        /// <param name="speak">a list of speech phrases. One will be chosen at random and spoken</param>
        /// <param name="from">the lower number in the range</param>
        /// <param name="to">the higher number in the range</param>
        /// <param name="onRecognize">when a number in the range is recognized, this action is triggered, passing in the number</param>
        /// <param name="canBeCancelled">whether this command can be cancelled or not</param>
        protected void speakRandom_ListenForNumberRange(
            string[] speak, 
            int from, 
            int to, 
            Action<int> onRecognize, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            Random r = new Random();
            this.speak_ListenForNumberRange(speak[r.Next(speak.Length)], from, to, onRecognize, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of phrases, and listen for a command phrase
        /// </summary>
        /// <param name="speak">a list of speech phrases. One will be chosen at random and spoken</param>
        /// <param name="onRecognize">when a command phrase is recognized, this event is triggered passing in the command phrase</param>
        /// <param name="helpText">Optional help text</param>
        /// <param name="canBeCancelled">whether or not this command can be cancelled</param>
        protected void speakRandom_ListenForCommandPhrase(
            string[] speak, 
            Action<string> onRecognize, 
            string helpText = "I am waiting for you to tell me a command.", 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            Random r = new Random();
            this.speak_ListenForCommandPhrase(speak[r.Next(speak.Length)], onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a category of phrases, and listen for one phrase from a list of phrases
        /// </summary>
        /// <param name="category">the category</param>
        /// <param name="listenFor">a list of phrases to listen for</param>
        /// <param name="onRecognize">when one of the phrases is recognized, this action is triggered, passing in the phrase</param>
        /// <param name="helpText">optional help text to be spoken when the user says "help"</param>
        /// <param name="canBeCancelled">whether or not this command can be cancelled</param>
        protected void speakCategory_ListenFor(
            string category, 
            string[] listenFor, 
            Action<string> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speakRandom_ListenFor(this.getSpeechPhrasesByCategory(category).ToArray(), listenFor, onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a category of phrases, and listen for anything
        /// </summary>
        /// <param name="category">the category</param>
        /// <param name="onRecognize">when a phrase is recognized, this action is triggered, passing in the phrase</param>
        /// <param name="helpText">optional help text, to be spoken when the user says "help"</param>
        /// <param name="canBeCancelled">whether or not this command can be cancelled</param>
        protected void speakCategory_ListenForAnything(
            string category, 
            Action<string> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speakRandom_ListenForAnything(this.getSpeechPhrasesByCategory(category).ToArray(), onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of category phrases, and listen for acceptance (yes or no)
        /// </summary>
        /// <param name="category">the category</param>
        /// <param name="onRecognize">when yes or no is recognized, this action is triggered, passing in true (yes) or false (no)</param>
        /// <param name="helpText">optional help text, to be spoken when the user says "help"</param>
        /// <param name="canBeCancelled">whether or not this command can be cancelled</param>
        protected void speakCategory_ListenForAcceptance(
            string category, 
            Action<bool> onRecognize, 
            string helpText = "I am waiting for you to say yes or no.", 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speakRandom_ListenForAcceptance(this.getSpeechPhrasesByCategory(category).ToArray(), onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of category phrase, and listen for a data list.
        /// 
        /// This allows you to use a DataList object to specify a list of phrases that you are looking for. For example, listen for the "name" values of notes.
        /// </summary>
        /// <param name="category">the category</param>
        /// <param name="dataList">a DataList instance</param>
        /// <param name="onRecognize">when a phrase from the list is recognized, this action is triggered, passing in the phrase</param>
        /// <param name="helpText">optional help text, to be spoken when the user says "help"</param>
        /// <param name="canBeCancelled">whether or not this command can be cancelled</param>
        protected void speakCategory_ListenForDataList(
            string category, 
            DataList dataList, 
            Action<string> onRecognize, 
            string helpText = null, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speakCategory_ListenFor(category, dataList.getList().ToArray(), onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of category phrases, and listen for a number between a number range.
        /// 
        /// Note that there is no "helpText" argument to this method. Instead, when "help" is initiated, it will say:
        /// "I am waiting for a number between from and to"
        /// </summary>
        /// <param name="category">the category</param>
        /// <param name="from">the lowest number in the range</param>
        /// <param name="to">the highest number in the range</param>
        /// <param name="onRecognize">when a number in the range is recognized, this action is triggered, passing in the number</param>
        /// <param name="canBeCancelled">whether or not this command can be cancelled</param>
        protected void speakCategory_ListenForNumberRange(
            string category, 
            int from, 
            int to, 
            Action<int> onRecognize, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speakRandom_ListenForNumberRange(this.getSpeechPhrasesByCategory(category).ToArray(), from, to, onRecognize, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from a list of category phrases, and listen for a command phrase
        /// </summary>
        /// <param name="category">the category</param>
        /// <param name="onRecognize">when a command phrase is recognized, this event is triggered, passing in the command phrase</param>
        /// <param name="helpText">optional help text</param>
        /// <param name="canBeCancelled">whether or not this command can be cancelled</param>
        protected void speakCategory_ListenForCommandPhrase(
            string category, 
            Action<string> onRecognize, 
            string helpText = "I am waiting for you to tell me a command.", 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speakRandom_ListenForCommandPhrase(this.getSpeechPhrasesByCategory(category).ToArray(), onRecognize, helpText, canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Confirm a phrase, and listen for acceptance.
        /// 
        /// This command basically says: "I got: phrase. Is this correct?"
        /// </summary>
        /// <param name="phrase">the phrase to confirm</param>
        /// <param name="onRecognize">when yes or no is recognized, this action is triggered, passing in true (yes) or false (no)</param>
        /// <param name="canBeCancelled">whether or not this phrase can be cancelled</param>
        protected void confirmPhrase_ListenForAcceptance(
            string phrase, 
            Action<bool> onRecognize, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speak_ListenForAcceptance("I got: " + phrase + ". Is this correct?", onRecognize, "I am waiting for you to tell me if: " + phrase + ": is correct.", canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak a random phrase from the "continue" category, and listen for acceptance
        /// </summary>
        /// <param name="onRecognize">when yes or no is recognized, this action is triggered, passing in true (yes) or false (no)</param>
        /// <param name="canBeCancelled">whether or not this command can be cancelled</param>
        protected void speakContinue_ListenForAcceptance(
            Action<bool> onRecognize, 
            bool canBeCancelled = true,
            TimeSpan? timeout = null,
            Action onTimeout = null
            ) {
            this.speakCategory_ListenForAcceptance("continue", onRecognize, "I am waiting for you to tell me if you want me to continue or not.", canBeCancelled, timeout, onTimeout);
        }

        /// <summary>
        /// Speak using a PromptBuilder instance
        /// </summary>
        /// <param name="promptBuilder">a PromptBuilder instance</param>
        protected void speak(PromptBuilder promptBuilder) {
            Core.instance.speak(promptBuilder);
        }

        /// <summary>
        /// Speak a line of text
        /// </summary>
        /// <param name="text">the line of text</param>
        protected void speak(string text) {
            if (text.Length > 0) {
                Core.instance.speak(string.Format(text, this.getUserName(), this.getComputerName()));
            }            
        }

        /// <summary>
        /// Speak a random phrase from a list of phrases
        /// </summary>
        /// <param name="texts">the list of phrases</param>
        protected void speakRandom(string[] texts) {
            if (texts.Length > 0) {
                Random r = new Random();
                this.speak(texts[r.Next(texts.Length)]);
            }            
        }

        /// <summary>
        /// Speak a random phrase from a category of phrases
        /// </summary>
        /// <param name="category">the category</param>
        protected void speakCategory(string category) {
            this.speakRandom(this.getSpeechPhrasesByCategory(category).ToArray());
        }

        /// <summary>
        /// Speak a phrase from the "okay" category
        /// </summary>
        protected void speakOkayCategory() {
            this.speakCategory("okay");
        }

        /// <summary>
        /// Speak a phrase from the "great" category
        /// </summary>
        protected void speakGreatCategory() {
            this.speakCategory("great");
        }

        /// <summary>
        /// Speak a phrase from the "whatever" category
        /// </summary>
        protected void speakWhateverCategory() {
            this.speakCategory("great");
        }

        /// <summary>
        /// Speak a list of phrases from a data list, and paginate based on the limit of the DataList.
        /// 
        /// This can be useful to speak a list of the "name" values of notes for example
        /// </summary>
        /// <param name="dataList">A DataList instance</param>
        /// <param name="onComplete">When the list has completed, this action is triggered</param>
        protected void speakDataList(DataList dataList, Action onComplete) {
            List<string> list = dataList.getList();
            if (list.Count > 0) {
                foreach (string s in list) {
                    this.speak(s + ",");
                }
                if (dataList.hasNextPage()) {
                    this.speakContinue_ListenForAcceptance(delegate (bool accepted) {
                        if (accepted) {
                            dataList.nextPage();
                            this.speakDataList(dataList, onComplete);
                        } else {
                            onComplete();
                        }
                    });
                } else {
                    onComplete();
                }
            } else {
                onComplete();
            }            
        }

        /// <summary>
        /// Speak a list of strings, and paginate based on the step value
        /// </summary>
        /// <param name="list">the list of strings</param>
        /// <param name="onComplete">when the list is completed, this action is triggered</param>
        /// <param name="step">the number of strings to speak before asking if the user wants to continue</param>
        protected void speakList(List<string> list, Action onComplete, int step = 10) {
            this.speakList(list.ToArray(), onComplete, step);
        }

        /// <summary>
        /// Speak a list of strings, and paginate based on the step value
        /// </summary>
        /// <param name="list">the list of strings</param>
        /// <param name="onComplete">when the list is completed, this action is triggered</param>
        /// <param name="step">the number of strings to speak before asking if the user wants to continue</param>
        protected void speakList(string[] list, Action onComplete, int step = 10) {
            this.speakList(list, onComplete, step, 0);
        }

        /// <summary>
        /// Used internally
        /// </summary>
        /// <param name="list"></param>
        /// <param name="onComplete"></param>
        /// <param name="step"></param>
        /// <param name="currentPage"></param>
        private void speakList(string[] list, Action onComplete, int step = 10, int currentPage = 0) {
            int start = currentPage * step;
            int end = start + step;
            if (end > list.Length) {
                end = list.Length;
            }

            for (int i = start; i < end; i++) {
                this.speak(list[i] + ",");
            }

            if (end >= list.Length) {
                onComplete();
            } else {
                this.speakContinue_ListenForAcceptance(delegate (bool accepted) {
                    if (accepted) {
                        currentPage++;
                        this.speakList(list, onComplete, step, currentPage);
                    } else {
                        onComplete();
                    }
                });
            }
        }

        /// <summary>
        /// Emulate recognize.
        /// </summary>
        /// <param name="text"></param>
        protected void emulateRecognize(string text) {
            Core.instance.emulateRecognize(text);
        }

        /// <summary>
        /// End the current command, and ask for another command.
        /// </summary>
        protected void end() {
            this.anotherCommand();
        }

        /// <summary>
        /// End the current command, and go to idle mode
        /// </summary>
        protected void endToIdle() {
            this.idle();
        }

        /// <summary>
        /// End the current command, and start a new command by it's command phrase
        /// </summary>
        /// <param name="commandPhrase">the command phrase</param>
        /// <returns>returns true or false based on whether the command was found and started</returns>
        protected bool endAndstartCommand(string commandPhrase) {
            Core core = Core.instance;
            core.isIdle = false;
            core.idleTime = null;
            if(core.cachedCommandPhrasesToCommands.ContainsKey(commandPhrase)) {
                core.cachedCommands[core.cachedCommandPhrasesToCommands[commandPhrase]].start(commandPhrase);
                return true;
            } else {
                this.idle();
                return false;
            }
        }

        /// <summary>
        /// Go to idle mode
        /// </summary>
        protected void idle() {
            Core core = Core.instance;
            core.isIdle = true;
            core.idleTime = DateTime.Now.AddSeconds(10);
            Core.instance.listenFor(new string[] { this.getComputerName() }, delegate (string text) {
                this.speakCategory("hello");
                this.silentCommand();
            });
        }

        /// <summary>
        /// Check whether a command event exists
        /// </summary>
        /// <param name="eventName">the event name</param>
        /// <returns>true if it exists, false if it doesn't</returns>
        protected bool commandEventExists(string eventName) {
            return Core.instance.commandEventExists(eventName);
        }

        /// <summary>
        /// Remove a command event
        /// </summary>
        /// <param name="eventName">the event name</param>
        protected void removeCommandEvent(string eventName) {
            Core.instance.removeCommandEvent(eventName);
        }

        /// <summary>
        /// Add a new command event
        /// </summary>
        /// <param name="commandEvent">the CommandEvent instance</param>
        protected void addCommandEvent(CommandEvent commandEvent) {
            Core.instance.addCommandEvent(commandEvent);
        }

        /// <summary>
        /// Add a new command event
        /// </summary>
        /// <param name="name">the event name</param>
        /// <param name="action">the action to trigger when the event is run</param>
        /// <param name="priority">the priority of the event. The higher the value, the higher the priority</param>
        /// <param name="startAfter">Start the event after a specific time. If this is null, the event will be started as soon as possible</param>
        /// <param name="expireAfter">When the event should expire (if it sits in the queue for too long). If this is null, it will never expire</param>
        protected void addCommandEvent(string name, Action action, int priority = 0, DateTime? startAfter = null, DateTime? expireAfter = null) {
            Core.instance.addCommandEvent(new CommandEvent(name, action, priority, startAfter, expireAfter));
        }

        /// <summary>
        /// used internally
        /// </summary>
        private void silentCommand() {
            Core core = Core.instance;
            core.isIdle = false;
            core.idleTime = null;
            this.speak_ListenForCommandPhrase("", delegate (string text) {
                if (!this.endAndstartCommand(text)) {
                    //fallback to idle
                    this.idle();
                }
            }, "I am waiting for a command. To hear a list of commands, say: list commands. To cancel, say: cancel.", true, TimeSpan.FromSeconds(10), delegate () {
                this.initialCommand();
            });
        }

        /// <summary>
        /// used internally
        /// </summary>
        private void initialCommand() {
            Core core = Core.instance;
            core.isIdle = false;
            core.idleTime = null;
            this.speakCategory_ListenForCommandPhrase("initial command", delegate (string text) {
                if(!this.endAndstartCommand(text)) {
                    //fallback to idle
                    this.idle();
                }
            }, "I am waiting for a command. To hear a list of commands, say: list commands. To cancel, say: cancel.");
        }

        /// <summary>
        /// used internally
        /// </summary>
        private void anotherCommand() {
            Core core = Core.instance;
            core.isIdle = false;
            core.idleTime = null;
            List<string> yes = this.getListenPhrasesByCategory("yes");
            List<string> no = this.getListenPhrasesByCategory("no");
            List<string> commands = core.cachedCommandPhrases;

            List<string> all = new List<string>();
            foreach (string s in yes) {
                all.Add(s);
            }
            foreach (string s in no) {
                all.Add(s);
            }
            foreach (string s in commands) {
                all.Add(s);
            }

            this.speakCategory_ListenFor("another command", all.ToArray(), delegate (string text) {
                //if yes
                if (yes.Contains(text)) {
                    this.initialCommand();
                }
                //if no
                else if (no.Contains(text)) {
                    this.speakCategory("okay");
                    this.idle();
                }
                //if command
                else if (commands.Contains(text)) {
                    if(!this.endAndstartCommand(text)) {
                        //fallback to idle
                        this.idle();
                    }
                } else {
                    //fallback to idle
                    this.idle();
                }
            }, "I am waiting for a command. To hear a list of commands, say: list commands. To cancel, say: cancel");
        }
    }
}