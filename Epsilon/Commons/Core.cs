﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading;
using System.IO;
using LitJson;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using Epsilon.Objects;
using Epsilon.Helpers;

namespace Epsilon.Commons {
    public class Core {
        public static readonly Core instance = new Core();

        private SpeechRecognitionEngine recognizer;
        private SpeechSynthesizer synthesizer;

        public SQLiteConnection database { get; private set; }

        public Meta cachedMeta { get; private set; }
        public Dictionary<int, CommandBase> cachedCommands { get; private set; } = new Dictionary<int, CommandBase>();
        public Dictionary<string, int> cachedCommandPhrasesToCommands { get; private set; } = new Dictionary<string, int>();
        public List<string> cachedCommandPhrases { get; private set; } = new List<string>();

        private Dictionary<string, CommandEvent> commandEvents = new Dictionary<string, CommandEvent>();

        public DateTime? idleTime = null;
        public bool isIdle = false;

        public string projectRoot;

        private Core() {

        }

        public void initiate() {
            this.projectRoot = Directory.GetCurrentDirectory() + "\\..\\..\\";

            this.synthesizer = new SpeechSynthesizer();

            //create the db connection
            this.database = new SQLiteConnection("Data Source=" + this.projectRoot + "Epsilon.sqlite" + ";Version=3;");
            this.database.Open();

            //cache the meta
            this.cachedMeta = JsonMapper.ToObject<Meta>(File.ReadAllText(this.projectRoot + "Meta.json"));

            //get the commands
            SQLiteDataReader r = new SQLiteCommand("SELECT id, nameSpace FROM Command", this.database).ExecuteReader();
            while(r.Read()) {
                //if the namespace exists
                Type t = Type.GetType((string)r["nameSpace"]);
                if(t != null) {
                    int id = Convert.ToInt32(r["id"]);

                    CommandBase cb = Activator.CreateInstance(t) as CommandBase;

                    //bind and store the phrases
                    foreach(string s in cb.getCommandPhrases()) {
                        if(!this.cachedCommandPhrases.Contains(s)) {
                            this.cachedCommandPhrases.Add(s);
                        }
                        this.cachedCommandPhrasesToCommands[s] = id;
                    }

                    //cache the command
                    this.cachedCommands[id] = cb;
                }
            }

            this.awake();

            ThreadPool.QueueUserWorkItem(this.update, null);
        }

        private void awake() {
            foreach(KeyValuePair<int, CommandBase> pair in this.cachedCommands) {
                pair.Value.awake();
            }
        }

        public void update(object context) {
            foreach(KeyValuePair<int, CommandBase> pair in this.cachedCommands) {
                pair.Value.update();
            }
            if(this.isIdle && this.idleTime.HasValue && this.idleTime < DateTime.Now) {
                this.handleCommandEvents();
            }
            Thread.Sleep(1000);
            this.update(context);
        }

        public void listenFor(string[] phrases, Action<string> onRecognize, TimeSpan? timeout = null, Action onTimeout = null) {
            //dispose the current recEngine
            if(this.recognizer != null) this.recognizer.Dispose();
            this.recognizer = new SpeechRecognitionEngine();
            Choices choices = new Choices();
            choices.Add(phrases);
            GrammarBuilder gb = new GrammarBuilder(choices);
            gb.Culture = Thread.CurrentThread.CurrentCulture;
            Grammar g = new Grammar(gb);
            this.recognizer.SetInputToDefaultAudioDevice();
            this.recognizer.LoadGrammar(g);
            if (!timeout.HasValue) {
                this.recognizer.InitialSilenceTimeout = TimeSpan.FromDays(99);
            } else {
                this.recognizer.InitialSilenceTimeout = timeout.Value;
                if (onTimeout != null) {
                    this.recognizer.RecognizeCompleted += delegate (object sender, RecognizeCompletedEventArgs e) {
                        onTimeout();
                    };
                } else {
                    Console.WriteLine("Waring: listening has timed out without an action to handle the timeout!");
                }
            }
            this.recognizer.RecognizeAsync(RecognizeMode.Single);

            this.recognizer.SpeechRecognized += delegate (object sender, SpeechRecognizedEventArgs e) {
                Console.WriteLine("heard: " + e.Result.Text);
                onRecognize(e.Result.Text);
            };

            //if the request gets rejected, try again
            this.recognizer.SpeechRecognitionRejected += delegate (object sender, SpeechRecognitionRejectedEventArgs e) {
                this.listenFor(phrases, onRecognize);
            };
        }

        public void listenForAnything(Action<string> onRecognize, TimeSpan? timeout = null, Action onTimeout = null) {
            //dispose the current recEngine
            if(this.recognizer != null) this.recognizer.Dispose();
            this.recognizer = new SpeechRecognitionEngine(Thread.CurrentThread.CurrentCulture);
            GrammarBuilder gb = new GrammarBuilder();
            gb.AppendDictation();
            gb.Culture = Thread.CurrentThread.CurrentCulture;
            Grammar g = new Grammar(gb);
            this.recognizer.SetInputToDefaultAudioDevice();
            this.recognizer.LoadGrammar(g);
            if (!timeout.HasValue) {
                this.recognizer.InitialSilenceTimeout = TimeSpan.FromDays(99);
            } else {
                this.recognizer.InitialSilenceTimeout = timeout.Value;
                if (onTimeout != null) {
                    this.recognizer.RecognizeCompleted += delegate (object sender, RecognizeCompletedEventArgs e) {
                        onTimeout();
                    };
                } else {
                    Console.WriteLine("Waring: listening has timed out without an action to handle the timeout!");
                }
            }
            this.recognizer.RecognizeAsync(RecognizeMode.Multiple);

            this.recognizer.SpeechRecognized += delegate (object sender, SpeechRecognizedEventArgs e) {
                Console.WriteLine("heard: " + e.Result.Text);
                onRecognize(e.Result.Text);
            };

            //if the request gets rejected, try again
            this.recognizer.SpeechRecognitionRejected += delegate (object sender, SpeechRecognitionRejectedEventArgs e) {
                this.listenForAnything(onRecognize);
            };
        }

        public void recordAnything(Action<RecognizedAudio> onRecognize) {
            //dispose the current recEngine
            if (this.recognizer != null) this.recognizer.Dispose();
            this.recognizer = new SpeechRecognitionEngine(Thread.CurrentThread.CurrentCulture);
            GrammarBuilder gb = new GrammarBuilder();
            gb.AppendDictation();
            gb.Culture = Thread.CurrentThread.CurrentCulture;
            Grammar g = new Grammar(gb);
            this.recognizer.SetInputToDefaultAudioDevice();
            this.recognizer.LoadGrammar(g);
            this.recognizer.InitialSilenceTimeout = TimeSpan.FromDays(99);
            this.recognizer.RecognizeAsync(RecognizeMode.Multiple);

            this.recognizer.SpeechRecognized += delegate (object sender, SpeechRecognizedEventArgs e) {
                onRecognize(e.Result.Audio);
            };

            //if the request gets rejected, try again
            this.recognizer.SpeechRecognitionRejected += delegate (object sender, SpeechRecognitionRejectedEventArgs e) {
                this.recordAnything(onRecognize);
            };
        }

        public void speak(PromptBuilder promptBuilder) {
            if(this.recognizer != null) {
                this.recognizer.RecognizeAsyncCancel();
            }
            this.synthesizer.Speak(promptBuilder);
        }

        public void speak(string text) {
            if(this.recognizer != null) {
                this.recognizer.RecognizeAsyncCancel();
            }
            Console.WriteLine("spoken: " + text);
            this.synthesizer.Speak(text);
        }

        public void emulateRecognize(string text) {
            this.recognizer.EmulateRecognize(text);
        }

        public void saveMeta() {
            File.WriteAllText(this.projectRoot + "Meta.json", JsonMapper.ToJson(this.cachedMeta));
        }

        private void handleCommandEvents() {
            if (this.commandEvents.Count > 0) {
                DateTime now = DateTime.Now;
                //work with a clone for thread safety
                Dictionary<string, CommandEvent> ces = new Dictionary<string, CommandEvent>(this.commandEvents);
                //find the highest priority event that can start
                string highest = "";
                int p = -1;
                foreach (KeyValuePair<string, CommandEvent> pair in ces) {
                    if (pair.Value.getPriority() > p && (!pair.Value.getStartAfter().HasValue || pair.Value.getStartAfter() < now)) {
                        highest = pair.Key;
                    }
                }
                if (highest.Length > 0) {
                    this.isIdle = false;
                    this.idleTime = null;
                    ces[highest].run();
                    //remove
                    this.removeCommandEvent(highest);
                } else {
                    //clear up any expired command events
                    foreach (KeyValuePair<string, CommandEvent> pair in ces) {
                        if (pair.Value.getExpiryAfter().HasValue && pair.Value.getExpiryAfter() < now) {
                            this.removeCommandEvent(pair.Key);
                        }
                    }
                }
            }
        }

        public bool commandEventExists(string eventName) {
            return this.commandEvents.ContainsKey(eventName);
        }

        public void removeCommandEvent(string eventName) {
            lock(this.commandEvents) {
                this.commandEvents.Remove(eventName);
            }
        }

        public void addCommandEvent(CommandEvent commandEvent) {
            lock(this.commandEvents) {
                if (!this.commandEvents.ContainsKey(commandEvent.getName())) {
                    this.commandEvents.Add(commandEvent.getName(), commandEvent);
                }
            }
        }
    }
}
